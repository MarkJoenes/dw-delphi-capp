unit ufrmimportfromexcel;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ufrmdbdialogbase, frxExportXLS, frxClass, frxExportPDF, Menus,
  UpPopupMenu, frxDesgn, frxDBSet, UpDataSource, DB, ADODB, UpAdoTable,
  UpAdoQuery, Buttons, UpSpeedButton, ExtCtrls, UPanel, StdCtrls, Grids,
  DBGrids, utypes, UpLabel, Wwdbigrd, Wwdbgrid, UpWWDbGrid;

type
  TFunImportExcelData = procedure(pqry:PUPAdoQuery) of Object;
  //导入数据回调函数
  TFunSetImpCountMsg = function(pqry:PUPAdoQuery) : string of object;
  //生成导入统计信息回调函数
  
  Tfrmimportexceldata = class(Tfrmdbdialogbase)
    p1: TPanel;
    lbl1: TLabel;
    edtwenjmc: TEdit;
    lbl2: TLabel;
    cbbsheets: TComboBox;
    btnopenexcel: TUpSpeedButton;
    dlgOpen: TOpenDialog;
    adoconexcel: TADOConnection;
    qryexcel: TADOQuery;
    dsexcel: TDataSource;
    pmexcel: TPopupMenu;
    pmns1: TMenuItem;
    btnopanpage: TSpeedButton;
    btnimport: TUpSpeedButton;
    pnlbottom: TUPanel;
    lblmsg: TUpLabel;
    dbgexcel: TUpWWDbGrid;
    procedure btnopenexcelClick(Sender: TObject);
    procedure pmns1Click(Sender: TObject);
    procedure btnopanpageClick(Sender: TObject);
    procedure btnimportClick(Sender: TObject);
    procedure dbgexcelCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
  private
  public
    ffun_importexceldata : TFunImportExcelData;
    //导入数据回调；
    ffun_SetImpCountMsg : TFunSetImpCountMsg;
    //数据集打开后的自动处理，通常用于统计记录条数信息；
    ffun_validataset : TFunSetImpCountMsg;
    //验证数据；在数据集打开后进行验证数据合法性；
  end;

var
  frmimportexceldata: Tfrmimportexceldata;

implementation

{$R *.dfm}

procedure Tfrmimportexceldata.btnopenexcelClick(Sender: TObject);
begin
  inherited;

  if Trim(edtwenjmc.Text) = '' then
  begin
    if dlgOpen.Execute then
    begin
      edtwenjmc.Text := dlgOpen.FileName;
    end;
  end;

  if Trim(edtwenjmc.Text) = '' then
  begin
    ShowMessage('请先选择Excel文件！');
    Exit;
  end;

  adoconexcel.Close;
  adoconexcel.ConnectionString := 'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=' +
    edtwenjmc.Text + ';Extended Properties=excel 8.0;Persist Security Info=False';
  adoconexcel.Open;

  cbbsheets.Items.Clear;
  adoconexcel.GetTableNames(cbbsheets.Items);
end;

procedure Tfrmimportexceldata.pmns1Click(Sender: TObject);
begin
  inherited;

  if not qryexcel.Active then Exit;
  if qryexcel.Eof then Exit;

  qryexcel.Delete;
end;

procedure Tfrmimportexceldata.btnopanpageClick(Sender: TObject);
var
  i : Integer;
  strmsg : string;
begin
  inherited;

  if cbbsheets.ItemIndex = -1 then
  begin
    ShowMessage('请选择Excel文件sheet页面！');
    cbbsheets.SetFocus;
    Exit;
  end;

  qryexcel.Close;
  qryexcel.SQL.Text := 'select * from [' + cbbsheets.Items[cbbsheets.itemindex] + ']';
  qryexcel.Open;


  for i:=0 to dbgexcel.GetColCount-1 do
  begin
    dbgexcel.ColWidths[i] := 150;
  end;

  if Assigned(ffun_SetImpCountMsg) then
  begin
    strmsg := ffun_SetImpCountMsg(@qryexcel);
    if strmsg = '' then
    begin
      pnlbottom.Visible := False;
    end
    else
    begin
      pnlbottom.Visible := True;
      lblmsg.Caption := strmsg;
    end;
  end;
end;

procedure Tfrmimportexceldata.btnimportClick(Sender: TObject);
begin
  inherited;

  if Assigned(ffun_importexceldata) then
    ffun_importexceldata(@qryexcel);
end;

procedure Tfrmimportexceldata.dbgexcelCalcCellColors(Sender: TObject;
  Field: TField; State: TGridDrawState; Highlight: Boolean; AFont: TFont;
  ABrush: TBrush);
begin
  inherited;

  case Field.DataSet.Fields[5].AsInteger of
    0:
    begin

    end;
    1: //未找见项目资料
    begin
      ABrush.Color := $00E8E8FF;
    end;
    9: //类别
    begin
      ABrush.Color := $00FFE1C4;
    end;
  end;

  if Highlight then
  begin
    AFont.Size := 12;
    AFont.Color := clBlue;
  end;
end;

end.
