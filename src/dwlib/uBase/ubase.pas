  //*******************************************************************
//  ProjectGroup1  pfuzhuang   
//  ubase.pas     
//                                          
//  创建: changym by 2012-01-03 
//        changup@qq.com                    
//  功能说明:                                            
//      所有业务类的基类;
//  修改历史:
//                                            
//  版权所有 (C) 2012 by changym
//                                                       
//*******************************************************************

unit ubase;

interface
uses          
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Math, Types, DateUtils, ADODB, utypes, HzSpell, Dialogs, ufrmcomm_showmessage,
  Nb30, winsock;

type
  tbase = class(TObject)
  private    
    ferrcode : integer; //错误代号
    ferrmsg : string;  //错误信息

  public
    { 数据类型合法性检测函数 }
    function strisdate(str : string) : boolean;
    function stristime(str : string) : boolean;
    function strisdatetime(str : string) : boolean;
    function strisint(str : string) : boolean;
    function strisfloat(str : string) : boolean;
    function striscolor(str : string) : Boolean;

    { 字符串操作函数 }
    function strfillchr_pre(str : string; c : char;
            splitchar : Char; count : integer) : string;
    function strfillchr_next(str : string; c : char; count : integer) : string;

    { Sql.Connectionstring取服务名、用户名、密码、数据库名; }
    function getDbLoingIngo(connectionstring : string;
        var dbserver, dbuser, dbpassword, dbdatabase : string) : Boolean;

    { 提示对话框 }
    function showmsg(caption : string; text : string; Flags: Longint) : boolean;
    //显示提示信息
    //参数：Flags
    //    0---普通提示;
    //    1---询问yes or no
    procedure showmessage(strmsg : string);
    //提示性消息
    procedure showwarning(strwarning : string);
    //告警性消息
    procedure showerror(strerrmsg : string);
    //错误性提示
    function showdialog(strcaption, strmsg : string) : Boolean;
    //用户选择性提示消息
    
    function timetocnstr(datetime : TDatetime; var str : widestring) : boolean;
    //时间转换为大写的字符串格式
    //12:00:23==十二时零分

    function get_jianpin(str : string) : string;
    //得到字符串str的汉字简拼
    function splitKV(str : string; csplit : char;
            var strkey : string; var strvalue : string) : Boolean;
    //分割由分隔符分割组成的字符串键值对
    function SplitString(const source, ch: string): TStringList;
    //字符串分割
    function SplitString_nulladd(const source, ch: string): TStringList;
    //字符串分割

    function getSelBeginDateTime(currdate : TDateTime) : string;
    //获得查询的开始时间;
    function getSelEndDateTime(currdate : TDateTime) : string;
    //获得查询的结束时间; 以+1秒返回;避开秒后n毫秒问题;
    
    function getCurrMonFirstDay(currdate : TDateTime) : TDateTime;
    function getCurrMonFirstDayTime(currdate : TDateTime) : TDateTime;
    //得到当月第一天
    function getCurrMonLastDay(currdate : TDateTime) : TDateTime;  
    //得到当月最后一天
    
    function getDateAs235959(currdate : TDateTime) : TDateTime;
    //当前日期的时分秒以23:59:59替换;

    function DatetimeToStr_DT(currdate : TDateTime) : string;
    //yyyy-mm-dd hh:nn:ss
    function DatetimeToStr_D(currdate : TDateTime) : string;
    //yyyy-mm-dd
    function DatetimeToStr_T(currdate : TDateTime) : string;
    //hh:nn:ss
    function TDateTimetoStr_nosplit(currdate : TDateTime) : string;
    //yyyymmddhhnnss  
    function DateTimetoStr_nosplit_24(currdate : TDateTime) : string;
    //yyyymmddhhnnss

    function baozsl2shul(baozsl : Double; zhuanhxs : integer) : Integer;
    //包装数量到数量转换
    function shul2baozsl(shul, zhuanhxs : Integer) : Double;
    //数量到包装数量转换

    function upstrtofloat(strvalue : string; len : integer; mode : TFPURoundingMode) : Double;
    //字符串转换为浮点数
    //len---保留小数位;-2两位;-3三位...
    //mode--模式;(rmNearest, rmDown, rmUp, rmTruncate);

    function up_jine_kunc(jine : Double) : Double;
    //库存类金额处理;和收现金不同处理,一般是库存保留3位小数,收现金是两位小数;
    function up_jine_shouxj(jine : Double) : Double;


    function floattostr_4(fl : Double) : string;
    function floattostr_3(fl : Double) : string;
    function floattostr_2(fl : Double) : string;
    function floattostr_1(fl : Double) : string;
    //小数点保留X位小数，后补0;


  { 网络相关
  }
  public
    function getMacAddress() : string;
    //获取机器mac地址             
    function dwGetHostName(): string;
    //获取机器名称
    function getIpByHostname(hostname: string): string;
    //根据主机名称获取ip地址
    function dwGetIp(isInternetIp: Boolean): string;
    //获取ip地址

    
  //控件初始化等操作相关函数
  public
    procedure TCheckListEdit_SelectAll(pcle : PUPCheckListEdit);
    procedure TCheckListEdit_UnSelectAll(pcle : PUPCheckListEdit);
    function TChecklistEdit_GetInsql(pcle : PUPCheckListEdit) : string;

  published                                             
    property errcode : integer read ferrcode write ferrcode;
    property errmsg : string read ferrmsg write ferrmsg;
  end;

implementation

{ tbase }

function tbase.baozsl2shul(baozsl: Double; zhuanhxs: integer): Integer;
begin
  Result := Round(baozsl * zhuanhxs);
end;

function tbase.getCurrMonFirstDay(currdate: TDateTime): TDateTime;
begin
  //日对日多一天;
  Result := currdate - DayOfTheMonth(currdate) + 1;
end;

function tbase.getCurrMonLastDay(currdate: TDateTime): TDateTime;
begin
  //倒回到下月初1再减去一天就是当月最后一天
  Result := getCurrMonFirstDay(IncMonth(currdate)) - 1;
end;

function tbase.getDbLoingIngo(connectionstring: string; var dbserver,
  dbuser, dbpassword, dbdatabase: string): Boolean;
var
  i, p : Integer;
  strupperconnstring :string;
begin
  Result := False;
  dbserver := '';
  dbuser := '';
  dbpassword := '';
  dbdatabase := '';    
  strupperconnstring := UpperCase(connectionstring);
  //Provider=SQLOLEDB.1;
  //Password=12345;
  //Persist Security Info=True;
  //User ID=sa;
  //Initial Catalog=uphis;
  //Data Source=.

  //get password
  i := Pos(UpperCase('Password='), strupperconnstring);
  if i = 0 then Exit;
  i := i + Length('Password=');
  for p:=i to Length(strupperconnstring) do
  begin
    if strupperconnstring[p] = ';' then
      Break;
  end;
  dbpassword := Copy(connectionstring, i, p-i);

  //get userid
  i := Pos(UpperCase('User ID='), strupperconnstring);
  if i = 0 then Exit;
  i := i + Length('User ID=');
  for p:=i to Length(strupperconnstring) do
  begin
    if strupperconnstring[p] = ';' then
      Break;
  end;
  dbuser := Copy(connectionstring, i, p-i);

  //get dbdatabase
  i := Pos(UpperCase('Initial Catalog='), strupperconnstring);
  if i = 0 then Exit;
  i := i + Length('Initial Catalog=');
  for p:=i to Length(strupperconnstring) do
  begin
    if strupperconnstring[p] = ';' then
      Break;
  end;
  dbdatabase := Copy(connectionstring, i, p-i);

  //get dbserver
  i := Pos(UpperCase('Data Source='), strupperconnstring);
  if i = 0 then Exit;  
  i := i + Length('Data Source=');
  for p:=i to Length(strupperconnstring) do
  begin
    if strupperconnstring[p] = ';' then
      Break;
  end;
  dbserver := Copy(connectionstring, i, p-i);

  Result := True;
end;

function tbase.get_jianpin(str: string): string;
begin   
  {$IFNDEF mainprog}
  result := THzSpell.PyHeadOfHz(str);
  {$endif}
end;

function tbase.showdialog(strcaption, strmsg: string): Boolean;
begin
  //Result := MessageDlg(strmsg, mtConfirmation, [mbYes, mbNo], 0) = mrYes;
                    
  { 采用自定义对话框了, 2012-7-22 by changym
  }
  if not Assigned(frmcomm_showmessage) then
    frmcomm_showmessage := Tfrmcomm_showmessage.Create(nil);
  frmcomm_showmessage.setmessage(strcaption, strmsg, 2);
  frmcomm_showmessage.ShowModal;
  Result := frmcomm_showmessage.ModalResult = mrOk;  
end;

procedure tbase.showerror(strerrmsg: string);
begin
  MessageDlg(strerrmsg, mtError, [mbOK], 0);

  { 采用自定义对话框了, 2012-7-22 by changym
  if not Assigned(frmcomm_showmessage) then
    frmcomm_showmessage := Tfrmcomm_showmessage.Create(nil);
  frmcomm_showmessage.setmessage('系统提示', '错误:'+chr(13)+chr($20)+strerrmsg, 1);
  frmcomm_showmessage.ShowModal;            
  }
end;

procedure tbase.showmessage(strmsg: string);
begin
  MessageDlg(strmsg, mtInformation, [mbOK], 0);

  { 采用自定义对话框了, 2012-7-22 by changym
  if not Assigned(frmcomm_showmessage) then
    frmcomm_showmessage := Tfrmcomm_showmessage.Create(nil);
  frmcomm_showmessage.setmessage('系统提示', '提示:'+chr(13)+chr($20)+strmsg, 1);
  frmcomm_showmessage.ShowModal;
  }
end;

function tbase.showmsg(caption, text: string; Flags: Integer): boolean;
begin
  Result := False;

  case Flags of
    0: //简单提示
    begin
      //MessageDlg(text, mtWarning, [mbOK], 0);
      //2012-7-22; 普通提示语转向showwarning
      showwarning(text);
      Exit;
    end;
    1: //询问yes or no
    begin
      Result := MessageDlg(text, mtConfirmation, [mbYes, mbNo], 0) = mrYes;
    end;
  end
end;

procedure tbase.showwarning(strwarning: string);
begin
  MessageDlg(strwarning, mtWarning, [mbOK], 0);

  { 采用自定义对话框了, 2012-7-22 by changym
  if not Assigned(frmcomm_showmessage) then
    frmcomm_showmessage := Tfrmcomm_showmessage.Create(nil);
  frmcomm_showmessage.setmessage('系统提示', '警告:'+chr(13)+chr($20)+strwarning, 1);
  frmcomm_showmessage.ShowModal;            
  }
end;

function tbase.shul2baozsl(shul, zhuanhxs: Integer): Double;
//var
//  zhengs, yus : Integer;
begin
  //Result := Trunc(shul div zhuanhxs);
  Result := RoundTo(shul / zhuanhxs, -3);
  {
  if zhuanhxs = 0 then
    raise Exception.Create('非法的转换系数！');

  zhengs := shul div zhuanhxs;
  yus := shul mod zhuanhxs;
  if yus > 0 then zhengs := zhengs + 1;
  Result := zhengs;
  Result := SimpleRoundTo(shul div zhuanhxs);
  }
end;

function tbase.splitKV(str: string; csplit: char; var strkey,
  strvalue: string): Boolean;
var
  i : Integer;
begin
  i := Pos(csplit, str);
  if i = 0 then
  begin
    result := false;
    exit;
  end;

  strkey := Copy(str, 0, i-1);
  strvalue := Copy(str, i+1, Length(str)-i);

  Result := True;
end;

function tbase.SplitString(const source, ch: string): TStringList;
var
  temp, t2: string;
  i: integer;
begin
  result := TStringList.Create;
  temp := source;
  i := pos(ch, source);
  while i <> 0 do
  begin
    t2 := copy(temp, 0, i - 1);
    if (t2 <> '') then
      result.Add(t2);
    delete(temp, 1, i - 1 + Length(ch));
    i := pos(ch, temp);
  end;
  //处理原串结尾最后一个字符不出ch的情况下剩余的尾巴部分；
  if not SameText(temp, '') then
    result.Add(temp);
end;

function tbase.strfillchr_next(str: string; c: char;
  count: integer): string;
begin

end;

function tbase.strfillchr_pre(str: string; c: char; splitchar : Char; 
  count: integer): string;  
var
  i : integer;
begin       
  Result := '';
  for i:=1 to count-length(str) do
  begin
    Result := Result + c;
  end;
  Result := Result + splitchar;
  Result := Result + str;
end;

function tbase.striscolor(str: string): Boolean;
var
  cl : TColor;
begin
  try
    cl := StringToColor(str);
    Result := True;
  except
    Result := False;
  end;
end;

function tbase.strisdate(str: string): boolean;
begin
  try
    strtodate(str);
    result := true;
  except
    result := false;
  end;
end;

function tbase.strisdatetime(str: string): boolean;
begin
  try
    strtodatetime(str);
    result := true;
  except
    result := false;
  end;
end;

function tbase.strisfloat(str: string): boolean;
begin
  try
    strtofloat(str);
    result := true;
  except
    result := false;
  end;
end;

function tbase.strisint(str: string): boolean;
begin
  try
    strtoint(str);
    result := true;
  except
    result := false;
  end;
end;

function tbase.stristime(str: string): boolean;
begin
  try
    strtotime(str);
    result := true;
  except
    result := false;
  end;
end;

function tbase.upstrtofloat(strvalue: string; len: integer;
  mode: TFPURoundingMode): Double;
begin
  SetRoundMode(mode);
  Result := RoundTo(StrToFloatDef(strvalue,0), len);
end;

function tbase.timetocnstr(datetime: TDatetime;
  var str: widestring): boolean;
var
  cn_num : array[0..10] of string;
  h,m : integer;
begin
  try
    cn_num[0] := '零';  
    cn_num[1] := '一';      
    cn_num[2] := '二';
    cn_num[3] := '三';
    cn_num[4] := '四';
    cn_num[5] := '五';
    cn_num[6] := '六';
    cn_num[7] := '七';
    cn_num[8] := '八';
    cn_num[9] := '九';

    h := hourof(datetime);
    m := MinuteOf(datetime);

    if h<10 then
    begin
      str := str + cn_num[h];
    end
    else
    begin
      str := str + cn_num[h div 10] + '十';
      if ((h mod 10) <> 0) then
      begin
        str := str + cn_num[h mod 10];
      end;
    end;
    str := str + '时';

    if m<10 then
    begin
      str := str + cn_num[m];
    end
    else
    begin
      str := str + cn_num[m div 10] + '十';
      if (m mod 10) <> 0 then
      begin
        str := str + cn_num[m mod 10];
      end;
    end;
    str := str + '分';

    result := true;
  except
    result := false;
  end;
end;

function tbase.up_jine_kunc(jine: Double): Double;
begin
  Result := RoundTo(jine, -4);
end;

function tbase.up_jine_shouxj(jine: Double): Double;
begin
  Result := SimpleRoundTo(jine);
  //Result := RoundTo(jine, -2);
end;

function tbase.getCurrMonFirstDayTime(currdate: TDateTime): TDateTime;
begin                                              
  //日对日多一天;
  Result := currdate - DayOfTheMonth(currdate) + 1;
  Result := StrToDateTime(DateToStr(Result) + ' 00:00:00');
end;

function tbase.getSelBeginDateTime(currdate: TDateTime): string;
begin
  Result := DateTimeToStr(currdate);
end;

function tbase.getSelEndDateTime(currdate: TDateTime): string;
begin
  //增加一秒后返回,避开12:23:45.789后的789问题;
  Result := DateTimeToStr(IncSecond(currdate));
end;

function tbase.getDateAs235959(currdate: TDateTime): TDateTime;
begin
  Result := StrToDateTime(DateToStr(currdate) + ' 23:59:59');
end;

procedure tbase.TCheckListEdit_SelectAll(pcle: PUPCheckListEdit);
var
  i : Integer;
begin
  for i:=0 to pcle^.Items.Count-1 do
  begin
    pcle^.Checked[i] := True;
  end;
end;

procedure tbase.TCheckListEdit_UnSelectAll(pcle: PUPCheckListEdit);
var
  i : Integer;
begin
  for i:=0 to pcle^.Items.Count-1 do
  begin
    pcle^.Checked[i] := False;
  end;
end;

function tbase.TChecklistEdit_GetInsql(pcle: PUPCheckListEdit): string;
var
  i : Integer;
  ifirst : Boolean;
begin
  Result := '';
  ifirst := True;

  if pcle^.Items.Count = 0 then Exit;
  
  for i:=0 to pcle^.Items.Count-1 do
  begin
    if pcle^.Checked[i] = True then
    begin
      if ifirst then
      begin
        Result := pcle^.GetItemValue(i);
        ifirst := False;
      end
      else
      begin
        Result := Result + ',' + pcle^.GetItemValue(i);
      end;
    end;
  end;

end;

function tbase.DatetimeToStr_D(currdate: TDateTime): string;
begin
  Result := FormatDateTime('yyyy-mm-dd', currdate);
end;

function tbase.DatetimeToStr_DT(currdate: TDateTime): string;
begin
  Result := FormatDateTime('yyyy-mm-dd hh:nn:ss', currdate);
end;

function tbase.DatetimeToStr_T(currdate: TDateTime): string;
begin
  Result := FormatDateTime('hh:nn:ss', currdate);
end;

function tbase.floattostr_1(fl: Double): string;
begin
  Result := FormatFloat('0.0', fl);
end;

function tbase.floattostr_2(fl: Double): string;
begin
  Result := FormatFloat('0.00', fl);
end;

function tbase.floattostr_3(fl: Double): string;
begin
  Result := FormatFloat('0.000', fl);
end;

function tbase.floattostr_4(fl: Double): string;
begin
  Result := FormatFloat('0.0000', fl);
end;

function tbase.getMacAddress: string;
var
  NCB: PNCB;
  Adapter: PAdapterStatus;
  URetCode: PChar;
  RetCode: char;
  I: integer;
  Lenum: PlanaEnum;
  _SystemID: string;
  TMPSTR: string;
begin
  Result := '';
  
  _SystemID := '';
  Getmem(NCB, SizeOf(TNCB));
  Fillchar(NCB^, SizeOf(TNCB), 0);

  Getmem(Lenum, SizeOf(TLanaEnum));
  Fillchar(Lenum^, SizeOf(TLanaEnum), 0);

  Getmem(Adapter, SizeOf(TAdapterStatus)); 
  Fillchar(Adapter^, SizeOf(TAdapterStatus), 0); 

  Lenum.Length := chr(0); 
  NCB.ncb_command := chr(NCBENUM); 
  NCB.ncb_buffer := Pointer(Lenum); 
  NCB.ncb_length := SizeOf(Lenum); 
  RetCode := Netbios(NCB);

  i := 0;
  repeat 
  Fillchar(NCB^, SizeOf(TNCB), 0); 
  Ncb.ncb_command := chr(NCBRESET); 
  Ncb.ncb_lana_num := lenum.lana[I]; 
  RetCode := Netbios(Ncb); 

  Fillchar(NCB^, SizeOf(TNCB), 0); 
  Ncb.ncb_command := chr(NCBASTAT); 
  Ncb.ncb_lana_num := lenum.lana[I]; 
  // Must be 16 
  Ncb.ncb_callname := ('*'); 

  Ncb.ncb_buffer := Pointer(Adapter); 

  Ncb.ncb_length := SizeOf(TAdapterStatus); 
  RetCode := Netbios(Ncb); 
  //---- calc _systemId from mac-address[2-5] XOR mac-address[1]... 
  if (RetCode = chr(0)) or (RetCode = chr(6)) then 
  begin 
  _SystemId := IntToHex(Ord(Adapter.adapter_address[0]), 2) + '-' + 
  IntToHex(Ord(Adapter.adapter_address[1]), 2) + '-' + 
  IntToHex(Ord(Adapter.adapter_address[2]), 2) + '-' + 
  IntToHex(Ord(Adapter.adapter_address[3]), 2) + '-' + 
  IntToHex(Ord(Adapter.adapter_address[4]), 2) + '-' + 
  IntToHex(Ord(Adapter.adapter_address[5]), 2); 
  end; 
  Inc(i); 
  until (I >= Ord(Lenum.Length)) or (_SystemID <> '00-00-00-00-00-00'); 
  FreeMem(NCB); 
  FreeMem(Adapter); 
  FreeMem(Lenum);
  result := _SystemID;
end;

function tbase.SplitString_nulladd(const source, ch: string): TStringList; 
var
  temp, t2: string;
  i: integer;
begin
  result := TStringList.Create;
  temp := source;
  i := pos(ch, source);
  while i <> 0 do
  begin
    t2 := copy(temp, 0, i - 1);
    result.Add(t2);
    
    delete(temp, 1, i - 1 + Length(ch));
    i := pos(ch, temp);
  end;
  //处理原串结尾最后一个字符不出ch的情况下剩余的尾巴部分；
  if not SameText(temp, '') then
    result.Add(temp);
end;

function tbase.TDateTimetoStr_nosplit(currdate: TDateTime): string;
begin
  Result := FormatDateTime('yyyymmddhhnnss', currdate);
end;

function tbase.DateTimetoStr_nosplit_24(currdate: TDateTime): string;
begin
  Result := FormatDateTime('yyyymmddhhnnss', currdate);
end;

function tbase.dwGetHostName: string;
var
 CNameBuffer : PChar;
 CLen : ^DWord;
begin
  GetMem(CNameBuffer,255);
  New(CLen);
  CLen^:=    255;
  if GetComputerName(CNameBuffer,CLen^) then
    result:=CNameBuffer
  else
    result:='';
  FreeMem(CNameBuffer,255);
  Dispose(CLen);
end;

function tbase.getIpByHostname(hostname: string): string;
var
  phe : pHostEnt;
  w : TWSAData;
  ip_address : longint;
  p : ^longint;
  ipstr : string;
begin
  result:= '0.0.0.0';
  if WSAStartup(2,w)<>0 then exit;
  
  phe:= gethostbyname(pchar(hostname));
  if phe<>nil then
  begin
    p:= pointer(phe^.h_addr_list^);
    ip_address:= p^;
    ip_address:= ntohl(ip_address);
    ipstr:= IntToStr(ip_address shr 24)+'.'+IntToStr((ip_address shr 16) and $ff)
      +'.'+IntToStr((ip_address shr 8) and $ff)+'.'+IntToStr(ip_address and $ff);
    Result :=ipstr;
  end;
end;

function tbase.dwGetIp(isInternetIp: Boolean): string;
type
  TaPInAddr = Array[0..10] of PInAddr;
  PaPInAddr = ^TaPInAddr;
var
  phe: PHostEnt;
  pptr: PaPInAddr;
  Buffer: Array[0..63] of Char;
  I: Integer;
  GInitData: TWSAData;
  IP: String;
begin
  Screen.Cursor := crHourGlass;
  Result:= '0.0.0.0';
  try
    WSAStartup($101, GInitData);
    IP:='0.0.0.0';
    
    GetHostName(Buffer, SizeOf(Buffer));
    phe := GetHostByName(buffer);
    if phe = nil then
    begin
      Exit;
    end;
    
    pPtr := PaPInAddr(phe^.h_addr_list);
    if isInternetIp then
    begin
      I := 0;
      while pPtr^[I] <> nil do
      begin
        IP := inet_ntoa(pptr^[I]^);
        Inc(I);
      end;
    end
    else
    begin
      IP := inet_ntoa(pptr^[0]^);
    end;
    
    WSACleanup;
    Result:=IP;//如果上网则为上网ip否则是网卡ip      
  finally
    Screen.Cursor := crDefault;
  end;
end;

end.
