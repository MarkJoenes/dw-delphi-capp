//*******************************************************************
//  pmyhisgroup  pdbgridSelfDraw   
//  udbgselfdraw_menzyisz.pas     
//                                          
//  创建: changym by 2012-08-03 
//        changup@qq.com                    
//  功能说明:                                            
//      门诊医生站dbg自画函数
//  修改历史:
//                                            
//  版权所有 (C) 2012 by changym
//                                                       
//*******************************************************************

unit udbgselfdraw_menzyisz;

interface

uses
  SysUtils,
  DB,
  Dialogs,
  Graphics,
  ADODB,
  Grids,
  Messages,
  Windows,
  Wwdbigrd,
  Wwdbgrid,
  UpWWDbGrid;
                              
//门诊; 医生站; 西药处方窗体; 明细编辑dbg
procedure dbgCalcCellColor_menz_yisz_xiycf_eddbg(Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush);

implementation

//门诊; 医生站; 西药处方窗体; 明细编辑dbg
procedure dbgCalcCellColor_menz_yisz_xiycf_eddbg(Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
  //组号; 按照组号奇偶数字错开显示不同的颜色
  if SameText(Field.FieldName, 'zuh') then
  begin
    if (Field.AsInteger mod 2) = 0 then
    begin
      ABrush.Color := clAqua;
    end
    else
    begin
      ABrush.Color := clFuchsia;
    end;
  end;

  //数量; zongl=0, 数量字段用红色特别显示;
  if SameText(Field.FieldName, 'zongl') then
  begin
    if Field.AsInteger = 0 then
    begin
      ABrush.Color := clRed;
    end;
  end;
end;

end.
