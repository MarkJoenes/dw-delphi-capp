unit udbgselfdraw_cxbb_yaok;

interface

uses
  SysUtils,
  DB,
  Dialogs,
  Graphics,
  ADODB,
  Grids,
  Messages,
  Windows,
  Wwdbigrd,
  Wwdbgrid,
  UpWWDbGrid;
                              
//药库入库单查询dbg1\dbg2
procedure dbgCalcCellColor_cxbb_yaok_rukd_dbg1(Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush);
procedure dbgCalcCellColor_cxbb_yaok_rukd_dbg2(Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush);

//出库单查询、dbg1、dbg2；  
procedure dbgCalcCellColor_cxbb_yaok_chukd_dbg1(Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush);
procedure dbgCalcCellColor_cxbb_yaok_chukd_dbg2(Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush);
            
//药房请领单dbg1、dbg2  
procedure dbgCalcCellColor_cxbb_yaok_qingld_dbg1(Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush);
procedure dbgCalcCellColor_cxbb_yaok_qingld_dbg2(Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush);
             
//药房退库单dbg1、dbg2
procedure dbgCalcCellColor_cxbb_yaok_tuikd_dbg1(Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush);
procedure dbgCalcCellColor_cxbb_yaok_tuikd_dbg2(Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush);

//出入库汇总表dbg  
procedure dbgCalcCellColor_cxbb_yaok_churkhzb_dbg(Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush);
                 
//出入库汇总表=按供应商    
procedure dbgCalcCellColor_cxbb_yaok_churkhzb_gongys_dbg(Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush);
                                
//出入库汇总表-按药品材料     
procedure dbgCalcCellColor_cxbb_yaok_churkhzb_yaopcl_dbg(Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush);
                           
//出入库汇总表-按药品类别      
procedure dbgCalcCellColor_cxbb_yaok_churkhzb_yaoplb_dbg(Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush);

//库存明细表
procedure dbgCalcCellColor_cxbb_yaok_kucmxb_dbg(Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush);

//库存明细汇总表    
procedure dbgCalcCellColor_cxbb_yaok_kucmxhzb_dbg(Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush);

//库存明细汇总表-按批号   
procedure dbgCalcCellColor_cxbb_yaok_kucmxhzb_pih_dbg(Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush);

//药品库存变动明细   
procedure dbgCalcCellColor_cxbb_yaok_yaop_kucbdmx_dbg(Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush);

//药品出入库明细表        
procedure dbgCalcCellColor_cxbb_yaok_churkmxb_dbg(Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush);

//药品调价记录dbg1；dbg2；
procedure dbgCalcCellColor_cxbb_yaok_tiaojjl_dbg1(Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush);
procedure dbgCalcCellColor_cxbb_yaok_tiaojjl_dbg2(Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush);



implementation
      
//药库入库单查询dbg1\dbg2
procedure dbgCalcCellColor_cxbb_yaok_rukd_dbg1(Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush); 
begin
end;
procedure dbgCalcCellColor_cxbb_yaok_rukd_dbg2(Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush); 
begin
end;

//出库单查询、dbg1、dbg2；  
procedure dbgCalcCellColor_cxbb_yaok_chukd_dbg1(Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
end;
procedure dbgCalcCellColor_cxbb_yaok_chukd_dbg2(Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush); 
begin
end;
            
//药房请领单dbg1、dbg2  
procedure dbgCalcCellColor_cxbb_yaok_qingld_dbg1(Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush); 
begin
end;
procedure dbgCalcCellColor_cxbb_yaok_qingld_dbg2(Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush);  
begin
end;
             
//药房退库单dbg1、dbg2
procedure dbgCalcCellColor_cxbb_yaok_tuikd_dbg1(Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush);   
begin
end;
procedure dbgCalcCellColor_cxbb_yaok_tuikd_dbg2(Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush);  
begin
end;

//出入库汇总表dbg  
procedure dbgCalcCellColor_cxbb_yaok_churkhzb_dbg(Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush); 
begin
end;
                 
//出入库汇总表=按供应商    
procedure dbgCalcCellColor_cxbb_yaok_churkhzb_gongys_dbg(Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush); 
begin
end;
                                
//出入库汇总表-按药品材料     
procedure dbgCalcCellColor_cxbb_yaok_churkhzb_yaopcl_dbg(Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush); 
begin
end;
                           
//出入库汇总表-按药品类别      
procedure dbgCalcCellColor_cxbb_yaok_churkhzb_yaoplb_dbg(Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush);  
begin
end;

//库存明细表
procedure dbgCalcCellColor_cxbb_yaok_kucmxb_dbg(Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
end;

//库存明细汇总表    
procedure dbgCalcCellColor_cxbb_yaok_kucmxhzb_dbg(Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush); 
begin
end;

//库存明细汇总表-按批号   
procedure dbgCalcCellColor_cxbb_yaok_kucmxhzb_pih_dbg(Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush); 
begin
end;

//药品库存变动明细   
procedure dbgCalcCellColor_cxbb_yaok_yaop_kucbdmx_dbg(Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush); 
begin
end;

//药品出入库明细表        
procedure dbgCalcCellColor_cxbb_yaok_churkmxb_dbg(Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush); 
begin
end;

//药品调价记录dbg1；dbg2；
procedure dbgCalcCellColor_cxbb_yaok_tiaojjl_dbg1(Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
end;
procedure dbgCalcCellColor_cxbb_yaok_tiaojjl_dbg2(Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
end;

end.
