program pmakeupdate;

uses
  Forms,
  ucsmsg in '..\classes\csmsg\ucsmsg.pas',
  ufrmmain in 'ufrmmain.pas' {frmmain},
  ubase in '..\lib\common\ubase.pas',
  uglobal in '..\lib\common\uglobal.pas',
  utypes in '..\lib\common\utypes.pas',
  ufrmbase in '..\lib\formbase\ufrmbase.pas' {frmbase},
  ufrmcomm_showmessage in '..\dllmoudle\common\ufrmcomm_showmessage.pas',
  umysystem in '..\lib\common\umysystem.pas',
  udbbase in '..\lib\common\udbbase.pas',
  udbconfigfile in '..\lib\common\udbconfigfile.pas',
  uencode in '..\lib\common\uencode.pas',
  utblyuang in '..\classes\dbtableclass\utblyuang.pas',
  HzSpell in '..\lib\moudle\huoqupinyin\HzSpell.pas',
  ukeyvaluelist in '..\lib\common\ukeyvaluelist.pas',
  umyconfig in '..\lib\common\umyconfig.pas',
  updos in '..\lib\common\updos.pas',
  ufrmcomm_showdialog in '..\dllmoudle\common\ufrmcomm_showdialog.pas' {frmcomm_showdialog};

{frmcomm_showmessage}

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(Tfrmmain, frmmain);
  Application.CreateForm(Tfrmcomm_showmessage, frmcomm_showmessage);
  Application.CreateForm(Tfrmcomm_showdialog, frmcomm_showdialog);
  if not frmmain.init() then
  begin
    frmmain.baseobj.showerror(frmmain.errmsg);
    application.Terminate;
    Exit;
  end;

  Application.Run;
end.
